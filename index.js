const express = require('express');
const app = express();
const cors = require('cors');
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
require('dotenv').config();

app.use(cors());

let result = "";

app.post('/', (req, res) => {
    result = req.body;
    return res.send(result);
});

app.get('/result', (req, res) => {
    return res.send(result);
});

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});